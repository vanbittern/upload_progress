function upload_progress_bytes_to_string(bytes) {
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1000)));
	return (bytes / Math.pow(1000, i)).toPrecision(3) + " " + ["B", "KB", "MB", "GB"][i];
}

Drupal.behaviors.upload_progress = {
	attach: function (context, settings) {
		
		jQuery(".image-widget-data input[type=submit]").each(function(el) {
			var ajax = Drupal.ajax[this.id];
			ajax.progress.url = undefined;
			ajax.options.uploadProgress = function(event, partial, total, percentage) {
				ajax.progress.object.setProgress(percentage, Drupal.t("Uploading... (current of total)", {current: upload_progress_bytes_to_string(partial), total: upload_progress_bytes_to_string(total)}));
			}
		});

	}
}
