upload_progress module
======================

What is this?
----------------------
This is a little module for Drupal 7 that uses client-side JavaScript to display the progress of a running upload.
It does not require the uploadprogress PECL module or APC and works great on configurations without Apache/mod_php, i.e. FastCGI/fcgi on Apache or Nginx or Lighttpd.

How does it work?
----------------------
This module uses the [progressEvent]: https://developer.mozilla.org/en-US/docs/Web/API/ProgressEvent fired by XMLHttpRequest in order to update the upload progress bar.


Caveats
----------------------
This module replaces the jquery.form library shipped by Drupal 7 with the most recent one from [malsup]: http://malsup.com/jquery/form/
However, no issues related to this are known so far.

License
----------------------
This module was made by [vanbittern.com]: https://vanbittern.com and licensed under the GNU GPLv2. You can find the full license in LICENSE.txt
